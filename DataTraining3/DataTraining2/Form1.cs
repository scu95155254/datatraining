﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Windows.Forms;
using DownloadSave;

namespace DataTraining2
{
    /// <summary>
    /// 資料訓練-第四階段
    /// </summary>
    public partial class Form1 : Form
    {
        /// <summary>
        /// import dll符合的所有Webcrawler
        /// </summary>
        [ImportMany]
        public IEnumerable<WebCrawler> WebCrawlers { get; set; }

        /// <summary>
        /// WinForm建構子
        /// </summary>
        public Form1()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 下拉選單
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void ComboBox1_Click(object sender, EventArgs e)
        {
            Compose();
            List<string> comboBoxData = new List<string>();

            foreach (var webcrawler in WebCrawlers)
            {
                comboBoxData.Add(webcrawler.TableName);
            }
            comboBox1.DataSource = comboBoxData;
        }

        /// <summary>
        /// 組合擴充元件讓這支程式可以使用
        /// </summary>
        public void Compose()
        {
            //目錄是使從某些來源發現的可用零件的對象。
            DirectoryCatalog catalog = new DirectoryCatalog(Environment.CurrentDirectory, "*.dll");
 

            CompositionContainer container = new CompositionContainer(catalog);

            //將零件(part)和主程式新增到組合容器
            container.ComposeParts(this);

    
        }
   
        /// <summary>
        /// 按下執行
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void Button1_Click(object sender, EventArgs e)
        {
            foreach (var webCrawler in WebCrawlers)
            {
                if (webCrawler.TableName == comboBox1.Text)
                {
                    string tableName = webCrawler.TableName;
                    if (comboBoxAction.Text == "新增、異動")
                    {
                        string isFinisned = BoolToString(webCrawler.DownloadFlow());
                        WriteLog($"{tableName}下載 {isFinisned}");
                        ShowTextBox($"{tableName}下載 {isFinisned}");
                        isFinisned = BoolToString(webCrawler.Check());
                        ShowTextBox($"{tableName}檢查 {isFinisned}");
                        WriteLog($"{tableName}檢查 {isFinisned}");
                        isFinisned = BoolToString(webCrawler.SaveOriginalFile());
                        ShowTextBox($"{tableName}儲存 {isFinisned}");
                        WriteLog($"{tableName}儲存 {isFinisned}");
                        isFinisned = BoolToString(webCrawler.ReadOriginalFile());
                        ShowTextBox($"{tableName}讀取原始檔 {isFinisned}");
                        WriteLog($"{tableName}讀取原始檔 {isFinisned}");
                        isFinisned = BoolToString(webCrawler.RegexFile());
                        ShowTextBox($"{tableName}正規化 {isFinisned}");
                        WriteLog($"{tableName}正規化 {isFinisned}");
                        isFinisned = BoolToString(webCrawler.SaveCSV());
                        ShowTextBox($"{tableName}儲存CSV {isFinisned}");
                        WriteLog($"{tableName}儲存CSV {isFinisned}");
                        isFinisned = BoolToString(webCrawler.ReadCSVFileChangeToDataTable());
                        ShowTextBox($"{tableName}讀取CSV檔 {isFinisned}");
                        WriteLog($"{tableName}讀取CSV檔 {isFinisned}");
                        isFinisned = BoolToString(webCrawler.UpdateDb());
                        ShowTextBox($"{tableName}存到SQL {isFinisned}");
                        WriteLog($"{tableName}存到SQL {isFinisned}");
                    }
                    if (comboBoxAction.Text == "清空")
                    {
                        string isFinisned = BoolToString(webCrawler.DeleteSqlData());
                        WriteLog($"{tableName}清空 {isFinisned}");
                        ShowTextBox($"{tableName} 清空 {isFinisned}");
                    }
                }
            }

        }

        /// <summary>
        /// 按下下拉選單
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void ComboBoxAction_Click(object sender, EventArgs e)
        {
            comboBoxAction.DataSource = new List<string> { Global.ADD, Global.REMOVE };
        }

        /// <summary>
        /// 紀錄LOG
        /// </summary>
        /// <param name="message">訊息來源</param>
        private void WriteLog(string message)
        {
            string dirName = AppDomain.CurrentDomain.BaseDirectory + @"\Log\";

            string fileName = dirName + DateTime.Now.ToString("yyyyMMdd") + ".txt";

            if (!Directory.Exists(dirName))
            {
                Directory.CreateDirectory(dirName);
            }
            if (!File.Exists(fileName))
            {
                // The File.Create method creates the file and opens a FileStream on the file. You neeed to close it.
                File.Create(fileName).Close();
            }
            using (StreamWriter sw = File.AppendText(fileName))
            {
                Log(message, sw);
            }
        }

        /// <summary>
        /// 真正的寫入LOG
        /// </summary>
        /// <param name="logMessage">訊息</param>
        /// <param name="tw">寫入器</param>
        private void Log(string logMessage, TextWriter tw)
        {
            tw.Write("\r\n Log Entry: ");
            tw.WriteLine("{0} {1}", DateTime.Now, DateTime.Now.ToLongDateString());
            tw.WriteLine();
            tw.WriteLine(" Message:{0}", logMessage);
            tw.WriteLine("--------------------");
        }

        /// <summary>
        /// 回傳成功或失掰
        /// </summary>
        /// <param name="isFinished">布林值</param>
        /// <returns></returns>
        private string BoolToString(bool isFinished)
        {
            if (isFinished)
            {
                return "成功";
            }
            return "失敗";
        }

        /// <summary>
        /// 顯示在textbox
        /// </summary>
        /// <param name="inform">資訊來源</param>
        private void ShowTextBox(string inform)
        {
            this.textBox1.AppendText($"{ inform}\r\n");
        }
    }
}
