﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DownloadSave
{
    [InheritedExport]
    public interface ISave
    {
        Boolean Save(string path, string data);
    }
}
