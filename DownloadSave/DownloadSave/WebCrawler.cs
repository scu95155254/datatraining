﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel.Composition;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace DownloadSave
{
    /// <summary>
    /// 中間參考的抽象類別，其他DLL實作這個類別
    /// </summary>
    [InheritedExport]
    public abstract class WebCrawler
    {
        /// <summary>
        /// 連線登入
        /// </summary>
        protected const string CONNECTION_INFO = "Data Source=192.168.10.180; Database=StockDB; User ID=test; Password=test";

        /// <summary>
        /// 網址
        /// </summary>
        protected string Url { get; set; }

        /// <summary>
        /// 儲存路徑
        /// </summary>
        protected string FilePath { get; set; }

        /// <summary>
        /// 資料表名稱
        /// </summary>
        public string TableName { get; protected set; }

        /// <summary>
        /// 原始檔資料格式
        /// </summary>
        protected string OriginalSvaeFormat { get; set; }

        /// <summary>
        /// 正規運算式
        /// </summary>
        protected string Pattern { get; set; }

        /// <summary>
        /// 具名群組的集合
        /// </summary>
        protected List<string> GroupName { get; set; }

        /// <summary>
        /// 把讀取的資料存成DataTable
        /// </summary>
        protected DataTable ReadDataTable { get; set; }

        /// <summary>
        /// 網頁內容下載成字串的資料，暫時存起來
        /// </summary>
        protected Dictionary<string, string> DownloadData { get; set; }

        /// <summary>
        /// 讀取本機的內容存成字串的資料
        /// </summary>
        protected Dictionary<string, string> ReadFileData { get; set; }

        /// <summary>
        /// 下載的檔案名稱
        /// </summary>
        protected List<string> FileName { get; set; }

        /// <summary>
        /// 建立暫存資料表
        /// </summary>
        protected string CreateTempTable { get; set; }

        /// <summary>
        /// SQL的Merge語法，要跟哪個表Merge
        /// </summary>
        protected string MergeTable { get; set; }

        /// <summary>
        /// 下載資料流程
        /// </summary>
        /// <returns>下載是否成功</returns>
        public abstract bool DownloadFlow();

        /// <summary>
        /// 確認是否需要存檔
        /// </summary>
        /// <returns>true需要存檔，false不需要</returns>
        public bool Check()
        {
            //判斷資料夾是否存在
            if (AddFolder(FilePath))
            {
                //資料夾存在，就要再判斷裡面的資料有沒有一樣
                return false;
            }
            //資料夾不存在，所以需要存檔
            return true;
        }

        /// <summary>
        /// 儲存原始資料
        /// </summary>
        /// <returns></returns>
        public bool SaveOriginalFile()
        {
            FileName = new List<string>();
            FileName.AddRange(DownloadData.Keys);
            try
            {
                foreach (string fileName in DownloadData.Keys)
                {
                    string path = $"{FilePath}\\{fileName}.{OriginalSvaeFormat}";
                    using (StreamWriter streamWriter = new StreamWriter(path, false, Encoding.UTF8))
                    {
                        streamWriter.Write(DownloadData[fileName]);
                    }
                }
                return true;
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
                return false;
            }
        }

        /// <summary>
        /// 讀取原始檔
        /// </summary>
        /// <returns>讀取是否成功</returns>
        public bool ReadOriginalFile()
        {
            ReadFileData = new Dictionary<string, string>();
            try
            {
                foreach (string path in Directory.GetFiles(FilePath))
                {
                    using (StreamReader streamReader = new StreamReader(path))
                    {
                        ReadFileData.Add(Path.GetFileNameWithoutExtension(path), streamReader.ReadToEnd());
                    }
                }
                return true;
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
                return false;
            }
        }

        /// <summary>
        /// 正規化資料
        /// </summary>
        /// <returns>是否正規化資料成功</returns>
        public abstract bool RegexFile();

        /// <summary>
        /// 存成CSV檔
        /// </summary>
        /// <returns>是否儲存成功</returns>
        public abstract bool SaveCSV();

        /// <summary>
        /// 設定DataTable的欄位
        /// </summary>
        protected abstract void SetDataTable();

        /// <summary>
        /// 讀取CSV檔 
        /// </summary>
        /// <returns>是否更新成功</returns>
        public bool ReadCSVFileChangeToDataTable()
        {
            try
            {
                DateTime now = DateTime.Now;
                long nowUnixTime = DateTimeOffset.Now.ToUnixTimeSeconds();
                //設定DataTable的欄位
                SetDataTable();
                string filePath = $"{FilePath}\\{TableName}";
                foreach (string path in Directory.GetFiles(filePath))
                {                    
                    using (StreamReader streamReader = new StreamReader(path, Encoding.UTF8))
                    {
                        streamReader.ReadLine();
                        while (!streamReader.EndOfStream)
                        {
                            var eachLineStr = streamReader.ReadLine();
                            string[] rows = eachLineStr.Split(',');
                            //split string to array of string use seperator
                            DataRow dr = ReadDataTable.NewRow();
                            for (int i = 0; i < rows.Length; i++)
                            {
                                dr[i] = rows[i];
                            }
                            dr["CTIME"] = now;
                            dr["MTIME"] = DateTimeOffset.Now.ToUnixTimeSeconds();
                            ReadDataTable.Rows.Add(dr);
                        }
                    }
                }
                return true;
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
                return false;
            }
        }

        /// <summary>
        /// 更新資料庫
        /// </summary>
        /// <returns>是否更新成功</returns>
        public bool UpdateDb()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(CONNECTION_INFO))
                {
                    connection.Open();
                    SqlCommand sqlComn = new SqlCommand
                    {
                        Connection = connection
                    };

                    sqlComn.CommandText = CreateTempTable;
                    sqlComn.ExecuteNonQuery();

                    using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(connection))
                    {
                        sqlBulkCopy.DestinationTableName = "#Table_Stock";
                        sqlBulkCopy.WriteToServer(ReadDataTable);
                    }
                    sqlComn.CommandText = MergeTable;
                    sqlComn.ExecuteNonQuery();
                }
                return true;
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
                return false;
            }
        }

        /// <summary>
        /// 刪除資料表
        /// </summary>
        /// <returns>是否刪除成功</returns>
        public bool DeleteSqlData()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(CONNECTION_INFO))
                {
                    //開啟資料庫連線
                    connection.Open();

                    SqlCommand sqlComn = new SqlCommand
                    {
                        Connection = connection,
                    };
                    try
                    {
                        string selectTable = $"delete from {TableName}";
                        sqlComn.CommandText = selectTable;
                        sqlComn.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
                return true;
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
                return false;
            }
        }

        /// <summary>
        /// 新增資料夾
        /// </summary>
        /// <param name="file">資料夾名稱</param>
        /// <returns>是否存在資料夾</returns>
        protected bool AddFolder(string file)
        {
            if (!Directory.Exists(file))
            {
                Directory.CreateDirectory(file);
                return false;
            }
            return true;
        }

        /// <summary>
        /// 民國轉西元
        /// </summary>
        /// <param name="date">民國</param>
        /// <returns>西元</returns>
        protected string TimeFormatConversion(string date)
        {
            CultureInfo culture = new CultureInfo("zh-TW");
            culture.DateTimeFormat.Calendar = new TaiwanCalendar();
            DateTime dateTime = DateTime.Parse(date, culture);
            return dateTime.ToString("yyyyMMdd");
        }

        /// <summary>
        /// 轉換成CSV格式
        /// </summary>
        /// <param name="regexValues">兩個List的格式</param>
        /// <returns>CSV的格式</returns>
        protected List<string> ListToCsv(List<List<string>> regexValues)
        {
            List<string[]> stringArray = new List<string[]>();
            foreach (var group in regexValues)
            {
                string[] tmp = group.ToArray();
                stringArray.Add(tmp);
            }
            List<string> regexValuesToCsv = new List<string>();

            foreach (string[] value in stringArray)
            {
                regexValuesToCsv.Add(string.Join(",", value));
            }
            return regexValuesToCsv;
        }

        /// <summary>
        /// 寫成CSV檔
        /// </summary>
        /// <param name="filepath">要存的路徑</param>
        /// <param name="csvFile">要存成CSV的資料來源</param>
        /// <param name="rowName">標頭欄位的屬性</param>
        /// <returns>是否寫入成功</returns>
        protected bool WriteToCSV(string filepath, List<string> csvFile, string rowName)
        {
            try
            {
                using (var file = new StreamWriter(filepath))
                {
                    foreach (var row in csvFile)
                    {
                        rowName = $"{rowName}\n{row}";
                    }
                    file.Write(rowName);
                }
                return true;
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
                return false;
            }
        }

        /// <summary>
        /// 網路連線
        /// </summary>
        /// <param name="url">網址</param>
        /// <param name="param">參數</param>
        /// <param name="method">方法</param>
        /// <param name="encoding">編碼</param>
        /// <returns>網址內容</returns>
        protected string MyWebClient(string url, Dictionary<string, string> param, string method, Encoding encoding)
        {
            using (WebClient webClient = new WebClient())
            {
                //指定WebClient的編碼
                webClient.Encoding = encoding;

                if (method.Equals("get"))
                {
                    if (param != null)
                    {
                        foreach (string key in param.Keys)
                        {
                            webClient.QueryString.Add(key, param[key]);
                        }
                    }
                    return webClient.DownloadString(url);
                }
                else if (method.Equals("post"))
                {
                    NameValueCollection nameValueCollection = new NameValueCollection();
                    foreach (string key in param.Keys)
                    {
                        nameValueCollection.Add(key, param[key]);
                    }
                    //發送post請求的結果
                    var result = webClient.UploadValues(url, nameValueCollection);
                    
                    //將結果轉成uft-8的字串
                    return Encoding.UTF8.GetString(result);
                }
                else
                {
                    return "暫時不支援的請求方式";
                }
            }
        }

        /// <summary>
        /// 正規化多個具名群組
        /// </summary>
        /// <param name="input">字串來源</param>
        /// <param name="pattern">Regex</param>
        /// <param name="groupName">具名群組</param>
        /// <returns>每個具名群組的集合</returns>
        protected List<List<string>> RegexFileData(string input, string pattern, List<string> groupName)
        {
            List<List<string>> totalRow = new List<List<string>>();
            foreach (Match match in Regex.Matches(input, pattern))
            {
                List<string> row = new List<string>();
                foreach (string name in groupName)
                {
                    row.Add(match.Groups[name].Value);
                }
                totalRow.Add(row);
            }
            return totalRow;
        }

        /// <summary>
        /// Regex統一處理
        /// </summary>
        /// <param name="input">要正規化的字串</param>
        /// <param name="pattern">正規表達式</param>
        /// <param name="groupName">Group了那些值</param>
        /// <returns>回傳正規完後的集合</returns>
        protected Dictionary<string, List<string>> RegexParam(string input, string pattern, List<string> groupName)
        {
            Dictionary<string, List<string>> totalRow = new Dictionary<string, List<string>>();

            foreach (string name in groupName)
            {
                List<string> row = new List<string>();
                foreach (Match match in Regex.Matches(input, pattern))
                {
                    if (match.Groups[name].Value.Equals(string.Empty))
                    {
                        continue;
                    }
                    row.Add(match.Groups[name].Value);
                }
                totalRow.Add(name, row);
            }
            return totalRow;
        }

        //public abstract bool ReadXmlFile();
        //public abstract bool SaveXml();
    }
}
