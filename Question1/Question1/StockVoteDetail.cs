﻿using System;
using System.Text;
using DownloadSave;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Question1
{
    /// <summary>
    /// 新增股東會投票日明細
    /// </summary>
    public class StockVoteDetail : WebCrawler
    {
        /// <summary>
        /// 投票日期
        /// </summary>
        private const int VotingDate = 0;

        /// <summary>
        /// 股東會日期
        /// </summary>
        private const int MeetingDate = 5;

        /// <summary>
        /// 沒有召集人的證券名稱
        /// </summary>
        private const int NameSec = 4;

        /// <summary>
        /// 有召集人的證券名稱
        /// </summary>
        private const int NameOne = 2;

        /// <summary>
        /// Regex
        /// </summary>
        private Dictionary<string, List<string>> RegexData;

        /// <summary>
        /// 建構子
        /// </summary>
        public StockVoteDetail()
        {
            Url = "https://www.stockvote.com.tw/evote/login/index/meetingInfoMore.html";
            TableName = "股東會投票日明細_Hank";
            FilePath = $"Q1\\{DateTime.Now.ToString("yyyyMMdd")}";
            OriginalSvaeFormat = "html";
            Pattern = @"<td.*\s*(?<id>.*)\r\s*<a.*(\s+(?<name>.*)\((?<people>.*)召集\)|\s+(?<name2>.*)\r)\s*<\/a>\s*.*\s*.*\s*(?<meetingDate>.*)\r\s*<\/td>\s*.*\s*(?<votingFirst>\S*)\s.\s(?<votingLast>\S*)\s*.*\s*.*\s*.*\s*(?<agency>\S*)\s*.*\s*.*\s*.*\s*(?<phone>.*)\r\s*<\/.*";
            GroupName = new List<string>() { "votingFirst", "id", "name", "people", "name2", "meetingDate", "agency", "phone" };
            CreateTempTable = "create table #Table_Stock (投票日期 char(8), 證券代號 varchar(20), 證券名稱 nvarchar(20), 召集人 nvarchar(50), 股東會日期 char(8), 發行代理機構 nvarchar(20), 聯絡電話 varchar(20),CTIME datetime, MTIME int)";
            MergeTable = "merge into 股東會投票日明細_Hank as Target " +
                          //merge 主鍵相同的資料，主鍵有兩個 確認唯一性
                         "using #Table_Stock as Source on Target.投票日期 = Source.投票日期 and Target.證券代號=Source.證券代號 " +
                         //再來確認資料有沒有異動，如果有異動才做更新的動作
                         "when matched and Target.證券名稱 != Source.證券名稱 or Target.召集人 != Source.召集人 or Target.股東會日期 != Source.股東會日期 or Target.發行代理機構 != Source.發行代理機構 or Target.聯絡電話 != Source.聯絡電話 " +
                         "then update set Target.證券名稱 = Source.證券名稱 ,Target.召集人 = Source.召集人 ,Target.股東會日期 = Source.股東會日期 ," +
                         "Target.發行代理機構 = Source.發行代理機構 ,Target.聯絡電話 = Source.聯絡電話 ," +
                         //注意!!!只需要更新MTIME就好，CTIME不會變動
                         "Target.MTIME = Source.MTIME " +
                         //如果沒有符合的資料，做新增的動作
                         "when not matched then insert (投票日期,證券代號,證券名稱,召集人,股東會日期,發行代理機構,聯絡電話,CTIME,MTIME) " +
                         "values (Source.投票日期,Source.證券代號,Source.證券名稱,Source.召集人,Source.股東會日期,Source.發行代理機構,Source.聯絡電話,Source.CTIME,Source.MTIME);";
        }

        /// <summary>
        /// 下載html的流程
        /// </summary>
        /// <returns></returns>
        public override bool DownloadFlow()
        {
            DownloadData = new Dictionary<string, string>();
            try
            {
                //先撈取總頁數
                int totalPage = GetTotalPage();

                //page為頁數
                for (int page = 1; page <= totalPage; page++)
                {
                    Dictionary<string, string> param = new Dictionary<string, string>
                    {
                        { "meetinfo", page.ToString() }
                    };
                    //下載成字串
                    DownloadData.Add(page.ToString(), MyWebClient(Url, param, "get", Encoding.UTF8));
                }
                return true;
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
                return false;
            }
        }

        /// <summary>
        /// 正規化
        /// </summary>
        /// <returns>是否正規化成功</returns>
        public override bool RegexFile()
        {
            RegexData = new Dictionary<string, List<string>>();
            try
            {
                foreach (string data in ReadFileData.Keys)
                {
                    List<List<string>> totalRow = RegexFileData(ReadFileData[data], Pattern, GroupName);
             
                    foreach (var group in totalRow)
                    {
                        group[VotingDate] = TimeFormatConversion(group[VotingDate]);
                        group[MeetingDate] = TimeFormatConversion(group[MeetingDate]);
                        if (!group[NameSec].Equals(string.Empty))
                        {
                            group[NameOne] = group[NameSec];
                        }
                        group.RemoveAt(NameSec);
                    }
                    //把List<List<string>>轉成csv的形式List<string> Ex--->(value1,value2,value3,value4,...)
                    List<string> regexValuesToCsv = ListToCsv(totalRow);
                    RegexData.Add(data, regexValuesToCsv);
                }
                return true;
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
                return false;
            }
        }

        /// <summary>
        /// 存成CSV
        /// </summary>
        /// <returns>是否儲存成功</returns>
        public override bool SaveCSV()
        {
            try
            {
                string filePath = $"Q1\\{DateTime.Now.ToString("yyyyMMdd")}\\{TableName}";

                AddFolder(filePath);

                foreach (string page in RegexData.Keys)
                {
                    string path = $"{filePath}\\{page}.csv";

                    string rowName = "投票日期,證券代號,證券名稱,召集人,股東會日期,發行代理機構,聯絡電話";

                    WriteToCSV(path, RegexData[page], rowName);
                }
                return true;
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
                return false;
            }
        }

        /// <summary>
        /// 設定DataTable
        /// </summary>
        protected override void SetDataTable()
        {
            ReadDataTable = new DataTable();
            ReadDataTable.Columns.Add("投票日期", typeof(string));
            ReadDataTable.Columns.Add("證券代號", typeof(string));
            ReadDataTable.Columns.Add("證券名稱", typeof(string));
            ReadDataTable.Columns.Add("召集人", typeof(string));
            ReadDataTable.Columns.Add("股東會日期", typeof(string));
            ReadDataTable.Columns.Add("發行代理機構", typeof(string));
            ReadDataTable.Columns.Add("聯絡電話", typeof(string));
            ReadDataTable.Columns.Add("CTIME", typeof(DateTime));
            ReadDataTable.Columns.Add("MTIME", typeof(int));
        }

        /// <summary>
        /// 取得總頁數
        /// </summary>
        /// <returns>總頁數</returns>
        public int GetTotalPage()
        {
            string htmlSourceCode = MyWebClient(Url, null, "get", Encoding.UTF8);
            //規則運算式-能撈出頁數總共有幾頁
            string pattern = "頁次.\\d\\/(?<totalPage>\\d*)<td>";
            return int.Parse(RegexParam(htmlSourceCode, pattern, new List<string> { "totalPage" })["totalPage"].First());
        }

        //public override bool SaveXml()
        //{
        //    this.Path = $"Q1\\{DateTime.Now.ToString("yyyyMMdd")}\\{this.TableName}";

        //    AddFolder(this.Path);

        //    //每一頁
        //    for (int page = 1; page <= this.RegexDownloadData.Count; page++)
        //    {
        //        this.Path = $"Q1\\{DateTime.Now.ToString("yyyyMMdd")}\\{this.TableName}\\{page}.xml";

        //        using (XmlTextWriter xmlTextWriter = new XmlTextWriter(this.Path, Encoding.UTF8))
        //        {
        //            xmlTextWriter.Formatting = Formatting.Indented;

        //            xmlTextWriter.WriteStartDocument();

        //            xmlTextWriter.WriteComment($"{this.TableName} 第{page}頁");

        //            xmlTextWriter.WriteStartElement($"{this.TableName}");
        //            foreach (var data in this.RegexDownloadData[page - 1])
        //            {
        //                xmlTextWriter.WriteElementString("投票日期", data[0]);
        //                xmlTextWriter.WriteElementString("證券代號", data[1]);
        //                xmlTextWriter.WriteElementString("證券名稱", data[2]);
        //                xmlTextWriter.WriteElementString("召集人", data[3]);
        //                xmlTextWriter.WriteElementString("股東會日期", data[4]);
        //                xmlTextWriter.WriteElementString("發行代理機構", data[5]);
        //                xmlTextWriter.WriteElementString("聯絡電話", data[6]);
        //            }

        //            xmlTextWriter.WriteEndElement();

        //            xmlTextWriter.WriteEndDocument();

        //            xmlTextWriter.Flush();
        //        }
        //    }
        //    return true;
        //}

        //public override bool ReadXmlFile()
        //{
        //    //DataSet dataSet = new DataSet();
        //    //dataSet.ReadXml("C:\\Users\\user1\\Documents\\datatraining\\DataTraining2\\DataTraining2\\bin\\Debug\\Q1\\20201108\\股東會投票日明細\\1.xml");

        //    return true;
        //}
    }
}
