﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using DownloadSave;

namespace Question1
{
    /// <summary>
    /// 股東會投票資料表
    /// </summary>
    public class StockVoteTable : WebCrawler
    {
        /// <summary>
        /// 股東會日期
        /// </summary>
        private const int MeetingDate = 4;

        /// <summary>
        /// 投票起日
        /// </summary>
        private const int VotingFirst = 5;

        /// <summary>
        /// 投票迄日
        /// </summary>
        private const int VotingLast = 6;

        /// <summary>
        /// 有召集人的證券名稱
        /// </summary>
        private const int NameHavePeople = 1;

        /// <summary>
        /// 沒有召集人的證券名稱
        /// </summary>
        private const int NameNoPeople = 3;

        /// <summary>
        /// Regex
        /// </summary>
        private Dictionary<string, List<string>> RegexData;

        /// <summary>
        /// 建構子
        /// </summary>
        public StockVoteTable()
        {
            Url = "https://www.stockvote.com.tw/evote/login/index/meetingInfoMore.html";
            TableName = "股東會投票資料表_Hank";
            FilePath = $"Q1\\{DateTime.Now.ToString("yyyyMMdd")}";
            OriginalSvaeFormat = "html";
            Pattern = @"<td.*\s*(?<id>.*)\r\s*<a.*(\s+(?<name>.*)\((?<people>.*)召集\)|\s+(?<name2>.*)\r)\s*<\/a>\s*.*\s*.*\s*(?<meetingDate>.*)\r\s*<\/td>\s*.*\s*(?<votingFirst>\S*)\s.\s(?<votingLast>\S*)\s*.*\s*.*\s*.*\s*(?<agency>\S*)\s*.*\s*.*\s*.*\s*(?<phone>.*)\r\s*<\/.*";
            GroupName = new List<string>() { "id", "name", "people", "name2", "meetingDate", "votingFirst", "votingLast", "agency", "phone" };

            CreateTempTable = "create table #Table_Stock ( 證券代號 varchar(10), 證券名稱 nvarchar(20), 召集人 nvarchar(50), 股東會日期 char(8), 投票起日 char(8),投票迄日 char(8), 發行代理機構 nvarchar(20), 聯絡電話 varchar(20),CTIME datetime, MTIME int)";

            MergeTable = "merge into 股東會投票資料表_Hank as Target " +
                         //merge 主鍵相同的資料，主鍵有兩個 確認唯一性
                         "using #Table_Stock as Source on Target.證券代號 = Source.證券代號 and Target.股東會日期 = Source.股東會日期 " +
                         //再來確認資料有沒有異動，如果有異動才做更新的動作
                         "when  matched " +
                         "and Target.召集人 != Source.召集人 or Target.股東會日期 != Source.股東會日期 or Target.投票起日 != Source.投票起日 or Target.投票迄日 != Source.投票迄日 " +
                         "or Target.發行代理機構 != Source.發行代理機構 or Target.聯絡電話 != Source.聯絡電話 " +
                         "then update set " +
                         "Target.證券名稱 = Source.證券名稱 ,Target.召集人 = Source.召集人 ,Target.投票起日 = Source.投票起日 ,Target.投票迄日 = Source.投票迄日 ," +
                         "Target.發行代理機構 = Source.發行代理機構 ,Target.聯絡電話 = Source.聯絡電話 ," +
                         //注意!!!只需要更新MTIME就好，CTIME不會變動
                         "Target.MTIME = Source.MTIME " +
                         "when not matched then insert (證券代號,證券名稱,召集人,股東會日期,投票起日,投票迄日,發行代理機構,聯絡電話,CTIME,MTIME) " +
                         "values (Source.證券代號,Source.證券名稱,Source.召集人,Source.股東會日期,Source.投票起日,Source.投票迄日,Source.發行代理機構,Source.聯絡電話,Source.CTIME,Source.MTIME);";
        }

        /// <summary>
        /// 下載流程
        /// </summary>
        /// <returns>是否下載成功</returns>
        public override bool DownloadFlow()
        {
            DownloadData = new Dictionary<string, string>();
            try
            {
                int count = 0;
                int totalPage = 0;
                do
                {
                    //先撈取總頁數
                    totalPage = GetTotalPage();
                    count++;
                    //totalPage是-1表示沒資料，且讀取次數不是五次
                }
                while (totalPage == -1 && count != 5);

                //如果讀取了五次
                if (totalPage == -1 && count == 5)
                {
                    return false;
                }

                //page為頁數
                for (int page = 1; page <= totalPage; page++)
                {
                    Dictionary<string, string> param = new Dictionary<string, string>
                    {
                        { "meetinfo", page.ToString() }
                    };

                    //下載成字串
                    DownloadData.Add(page.ToString(), MyWebClient(Url, param, "get", Encoding.UTF8));
                }
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                return false;
            }
        }

        /// <summary>
        /// 解析資料，正規化
        /// </summary>
        /// <returns>是否正規化成功</returns>
        public override bool RegexFile()
        {
            RegexData = new Dictionary<string, List<string>>();
            try
            {
                foreach (string data in ReadFileData.Keys)
                {
                    List<List<string>> totalRow = RegexFileData(ReadFileData[data], Pattern, GroupName);
                    foreach (var group in totalRow)
                    {
                        group[MeetingDate] = TimeFormatConversion(group[MeetingDate]);
                        group[VotingFirst] = TimeFormatConversion(group[VotingFirst]);
                        group[VotingLast] = TimeFormatConversion(group[VotingLast]);
                        if (!group[NameNoPeople].Equals(string.Empty))
                        {
                            group[NameHavePeople] = group[NameNoPeople];
                        }
                        group.RemoveAt(NameNoPeople);
                    }
                    //把List<List<string>>轉成csv的形式List<string> Ex--->(value1,value2,value3,value4,...)
                    List<string> regexValuesToCsv = ListToCsv(totalRow);
                    RegexData.Add(data, regexValuesToCsv);
                }
                return true;
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
                return false;
            }
        }

        /// <summary>
        /// 存成CSV
        /// </summary>
        /// <returns>是否儲存成功</returns>
        public override bool SaveCSV()
        {
            try
            {
                string filePath = $"Q1\\{DateTime.Now.ToString("yyyyMMdd")}\\{this.TableName}";

                AddFolder(filePath);

                foreach (string page in RegexData.Keys)
                {
                    string path = $"{filePath}\\{page}.csv";

                    string rowName = "證券代號,證券名稱,召集人,股東會日期,投票起日,投票迄日,發行代理機構,聯絡電話";

                    WriteToCSV(path, RegexData[page], rowName);
                }
                return true;
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
                return false;
            }
        }

        /// <summary>
        /// 取得總頁數
        /// </summary>
        /// <returns>總頁數</returns>
        public int GetTotalPage()
        {
            try
            {
                string htmlSourceCode = MyWebClient(Url, null, "get", Encoding.UTF8);
                //規則運算式-能撈出頁數總共有幾頁
                string pattern = "頁次.\\d\\/(?<totalPage>\\d*)<td>";

                return int.Parse(RegexParam(htmlSourceCode, pattern, new List<string> { "totalPage" })["totalPage"].First());
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
                return -1;
            }
        }

        /// <summary>
        /// 設定DataTable
        /// </summary>
        protected override void SetDataTable()
        {
            ReadDataTable = new DataTable();
            ReadDataTable.Columns.Add("證券代號", typeof(string));
            ReadDataTable.Columns.Add("證券名稱", typeof(string));
            ReadDataTable.Columns.Add("召集人", typeof(string));
            ReadDataTable.Columns.Add("股東會日期", typeof(string));
            ReadDataTable.Columns.Add("投票起日", typeof(string));
            ReadDataTable.Columns.Add("投票迄日", typeof(string));
            ReadDataTable.Columns.Add("發行代理機構", typeof(string));
            ReadDataTable.Columns.Add("聯絡電話", typeof(string));
            ReadDataTable.Columns.Add("CTIME", typeof(DateTime));
            ReadDataTable.Columns.Add("MTIME", typeof(int));
        }
    }
}
