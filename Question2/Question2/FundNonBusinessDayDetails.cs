﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using DownloadSave;

namespace Question2
{
    /// <summary>
    /// 基金非營業日明細_Hank
    /// </summary>
    public class FundNonBusinessDayDetails : WebCrawler
    {
        /// <summary>
        /// 參數eventvalidation
        /// </summary>
        private const string EVENTVALIDATION = "eventvalidation";

        /// <summary>
        /// 參數viewstate
        /// </summary>
        private const string VIEWSTATE = "viewstate";

        /// <summary>
        /// 參數viewstategenerator
        /// </summary>
        private const string VIEWSTATEGENERATOR = "viewstategenerator";

        /// <summary>
        /// 參數year
        /// </summary>
        private const string YEAR = "year";

        /// <summary>
        /// 非營業日
        /// </summary>
        private const int DATE = 0;

        /// <summary>
        /// 基金名稱
        /// </summary>
        private const int NAME = 3;

        /// <summary>
        /// 第一筆資料
        /// </summary>
        private const int FIRSTDATA = 0;

        /// <summary>
        /// 後一筆資料
        /// </summary>
        private const int AFTERDATA = 1;

        /// <summary>
        /// 第一筆資料
        /// </summary>
        private const int SORTFIRST = 1;

        /// <summary>
        /// RegexData
        /// </summary>
        private Dictionary<string, List<string>> RegexData;

        /// <summary>
        /// 建構子
        /// </summary>
        public FundNonBusinessDayDetails()
        {
            Url = "https://www.sitca.org.tw/ROC/Industry/IN2107.aspx?pid=IN2213_03";
            TableName = "基金非營業日明細_Hank";
            FilePath = $"Q2";
            OriginalSvaeFormat = "html";
            Pattern = @"<td.*?>(?<date>.*?)<\/td><td.*?>(?<id>.*?)<\/td><td.*?>(?<uniformNumbers>.*?)<\/td><td.*?>(?<fundName>.*?)<\/td>\s*.*\s*";
            GroupName = new List<string>() { "date", "id", "uniformNumbers", "fundName" };
            CreateTempTable = "create table #Table_Stock (非營業日 char(8), 公司代號 varchar(10), 基金統編 varchar(10), 基金名稱 nvarchar(200), 排序 int ,CTIME datetime, MTIME int)";
            MergeTable = "merge into 基金非營業日明細_Hank as Target " +
                         //merge 主鍵相同的資料，主鍵有兩個 確認唯一性
                         "using #Table_Stock as Source on Target.非營業日 = Source.非營業日 and Target.基金統編 = Source.基金統編 " +
                         //再來確認資料有沒有異動，如果有異動才做更新的動作
                         "when matched and Target.公司代號 !=Source.公司代號 or Target.基金名稱 !=Source.基金名稱 or Target.排序 != Source.排序 " +
                         "then update set Target.公司代號 = Source.公司代號 ,Target.基金名稱 = Source.基金名稱 ,Target.排序 = Source.排序 ," +
                         //注意!!!只需要更新MTIME就好，CTIME不會變動
                         "Target.MTIME = Source.MTIME " +
                         "when not matched then insert (非營業日,公司代號,基金統編,基金名稱,排序,CTIME,MTIME) " +
                         "values (Source.非營業日,Source.公司代號,Source.基金統編,Source.基金名稱,Source.排序,Source.CTIME,Source.MTIME);";
        }

        /// <summary>
        /// 下載流程
        /// </summary>
        /// <returns></returns>
        public override bool DownloadFlow()
        {
            DownloadData = new Dictionary<string, string>();
            try
            {
                string htmlSourceCode = MyWebClient(Url, null, "get", Encoding.UTF8);
                //撈出我要的參數
                string pattern = @"<option.*value=""(?<year>.*)"".*?年<\/option>\s*|(id=""__EVENTVALIDATION"".*value=""(?<eventvalidation>.*)"")|(id=""__VIEWSTATE"".*value=""(?<viewstate>.*?)"")|(id=""__VIEWSTATEGENERATOR""\svalue=""(?<viewstategenerator>.*)"")";

                Dictionary<string, List<string>> nameValueParm = RegexParam(htmlSourceCode, pattern, new List<string>() { EVENTVALIDATION, VIEWSTATE, VIEWSTATEGENERATOR, YEAR });

                string eventvalidation = nameValueParm[EVENTVALIDATION].First();

                string viewstate = nameValueParm[VIEWSTATE].First();

                string viewstategenerator = nameValueParm[VIEWSTATEGENERATOR].First();

                string viewstateencrypted = string.Empty;

                List<string> eachYear = nameValueParm[YEAR];

                foreach (var year in eachYear)
                {
                    Dictionary<string, string> param = new Dictionary<string, string>
                    {
                        { "__VIEWSTATE", viewstate },
                        { "__VIEWSTATEGENERATOR", viewstategenerator },
                        { "__VIEWSTATEENCRYPTED", string.Empty },
                        { "__EVENTVALIDATION", eventvalidation },
                        { "ctl00$ContentPlaceHolder1$ddlQ_Year", year },
                        { "ctl00$ContentPlaceHolder1$ddlQ_Comid", string.Empty },
                        { "ctl00$ContentPlaceHolder1$BtnQuery", "查詢" }
                    };
                    //將結果轉成uft-8的字串
                    string resultData = MyWebClient(Url, param, "post", Encoding.UTF8);
                    DownloadData.Add(year, resultData);
                }
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        /// <summary>
        /// 正規化
        /// </summary>
        /// <returns>是否正規化成功</returns>
        public override bool RegexFile()
        {
            try
            {
                RegexData = new Dictionary<string, List<string>>();

                foreach (string year in ReadFileData.Keys)
                {
                    List<List<string>> regexValues = RegexFileData(ReadFileData[year], Pattern, GroupName).OrderByDescending(date => date[DATE]).ThenByDescending(name => name[NAME].Length).ToList();

                    int sort = SORTFIRST;

                    regexValues[FIRSTDATA].Add(sort.ToString());

                    for (int index = AFTERDATA; index < regexValues.Count; index++)
                    {
                        if (regexValues[index][DATE].Equals(regexValues[index - 1][DATE]))
                        {
                            if (regexValues[index][NAME].Length < regexValues[index - 1][NAME].Length)
                            {
                                regexValues[index].Add((++sort).ToString());
                            }
                            else
                            {
                                regexValues[index].Add(sort.ToString());
                            }
                        }
                        else
                        {
                            sort = 1;
                            regexValues[index].Add(sort.ToString());
                        }
                    }
                    //把List<List<string>>轉成csv的形式List<string> Ex--->(value1,value2,value3,value4,...)
                    List<string> regexValuesToCsv = ListToCsv(regexValues);
                    RegexData.Add(year, regexValuesToCsv);
                }

                return true;
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);

                return false;
            }
        }

        /// <summary>
        /// 存成CSV
        /// </summary>
        /// <returns>是否儲存成功</returns>
        public override bool SaveCSV()
        {
            try
            {
                string filePath = $"Q2\\{this.TableName}";
                AddFolder(filePath);
                foreach (string year in RegexData.Keys)
                {
                    string rowName = "非營業日,公司代號,基金統編,基金名稱,排序";
                    using (StreamWriter file = new StreamWriter($"{filePath}\\{year}.csv", false, Encoding.UTF8))
                    {
                        foreach (string row in RegexData[year])
                        {
                            rowName = $"{rowName}\n{row}";
                        }
                        file.Write(rowName);
                    }
                }
                return true;
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
                return false;
            }
        }

        /// <summary>
        /// 設定DataTable
        /// </summary>
        protected override void SetDataTable()
        {
            ReadDataTable = new DataTable();
            ReadDataTable.Columns.Add("非營業日", typeof(string));
            ReadDataTable.Columns.Add("公司代號", typeof(string));
            ReadDataTable.Columns.Add("基金統編", typeof(string));
            ReadDataTable.Columns.Add("基金名稱", typeof(string));
            ReadDataTable.Columns.Add("排序", typeof(string));
            ReadDataTable.Columns.Add("CTIME", typeof(DateTime));
            ReadDataTable.Columns.Add("MTIME", typeof(int));
        }
    }
}
