﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using DownloadSave;

namespace Question2
{
    /// <summary>
    /// 基金非營業日統計
    /// </summary>
    public class FundNonBusinessTable : WebCrawler
    {
        /// <summary>
        /// 非營業日
        /// </summary>
        private const int Date = 0;

        /// <summary>
        /// 公司代號
        /// </summary>
        private const int Id = 1;

        /// <summary>
        /// 正規化的資料
        /// </summary>
        private Dictionary<string, List<string>> RegexData;

        /// <summary>
        /// 建構子
        /// </summary>
        public FundNonBusinessTable()
        {
            Url = "https://www.sitca.org.tw/ROC/Industry/IN2107.aspx?pid=IN2213_03";
            TableName = "基金非營業日統計_Hank";
            FilePath = $"Q2";
            OriginalSvaeFormat = "html";
            Pattern = @"<td.*?>(?<date>.*?)<\/td><td.*?>(?<id>.*?)<\/td><td.*?>(?<uniformNumbers>.*?)<\/td><td.*?>(?<fundName>.*?)<\/td>\s*.*\s*";
            GroupName = new List<string>() { "date", "id", "uniformNumbers" };
            CreateTempTable = "create table #Table_Stock (非營業日 char(8), 公司代號 varchar(10),基金總數 int ,CTIME datetime, MTIME int)";
            MergeTable = "merge into 基金非營業日統計_Hank as Target " +
                //merge 主鍵相同的資料，主鍵有兩個 確認唯一性
                "using #Table_Stock as Source on Target.非營業日 = Source.非營業日 and Target.公司代號 = Source.公司代號 " +
                //再來確認資料有沒有異動，如果有異動才做更新的動作
                "when matched and Target.基金總數 != Source.基金總數 " +
                "then update set Target.基金總數 = Source.基金總數 ," +
                //注意!!!只需要更新MTIME就好，CTIME不會變動
                "Target.MTIME = Source.MTIME " +
                "when not matched then insert (非營業日,公司代號,基金總數,CTIME,MTIME) values (Source.非營業日,Source.公司代號,Source.基金總數,Source.CTIME,Source.MTIME);";
        }

        /// <summary>
        /// 下載流程
        /// </summary>
        /// <returns>是否下載成功</returns>
        public override bool DownloadFlow()
        {
            DownloadData = new Dictionary<string, string>();
            try
            {
                string htmlSourceCode = MyWebClient(Url, null, "get", Encoding.UTF8);

                //撈出我要的參數
                Dictionary<string, List<string>> nameValueParm = RegexParam(htmlSourceCode, @"<option.*value=""(?<year>.*)"".*?年<\/option>\s*|(id=""__EVENTVALIDATION"".*value=""(?<eventvalidation>.*)"")|(id=""__VIEWSTATE"".*value=""(?<viewstate>.*?)"")|(id=""__VIEWSTATEGENERATOR""\svalue=""(?<viewstategenerator>.*)"")", new List<string>() { "eventvalidation", "viewstate", "viewstategenerator", "year" });

                string eventvalidation = nameValueParm["eventvalidation"].First();

                string viewstate = nameValueParm["viewstate"].First();

                string viewstategenerator = nameValueParm["viewstategenerator"].First();

                string viewstateencrypted = string.Empty;

                List<string> eachYear = nameValueParm["year"];

                foreach (var year in eachYear)
                {
                    Dictionary<string, string> param = new Dictionary<string, string>
                    {
                        { "__VIEWSTATE", viewstate },
                        { "__VIEWSTATEGENERATOR", viewstategenerator },
                        { "__VIEWSTATEENCRYPTED", string.Empty },
                        { "__EVENTVALIDATION", eventvalidation },
                        { "ctl00$ContentPlaceHolder1$ddlQ_Year", year },
                        { "ctl00$ContentPlaceHolder1$ddlQ_Comid", string.Empty },
                        { "ctl00$ContentPlaceHolder1$BtnQuery", "查詢" }
                    };
                    //將結果轉成uft-8的字串
                    string resultData = MyWebClient(Url, param, "post", Encoding.UTF8);
                    DownloadData.Add(year, resultData);
                }
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        /// <summary>
        /// 正規化
        /// </summary>
        /// <returns>是否正規化成功</returns>
        public override bool RegexFile()
        {
            RegexData = new Dictionary<string, List<string>>();
            try
            {
                foreach (string year in ReadFileData.Keys)
                {
                    var regexData = RegexFileData(ReadFileData[year], Pattern, GroupName).GroupBy(value => new { date = value[Date], id = value[Id] }, value => value).Select(g => new { g.Key.date, g.Key.id, count = g.Count() }).ToList();
                    List<string> fundStatistics = new List<string>();
                    foreach (var column in regexData)
                    {
                        string row = $"{column.date},{column.id},{column.count}";
                        fundStatistics.Add(row);
                    }
                    RegexData.Add(year, fundStatistics);
                }
                return true;
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
                return false;
            }
        }

        /// <summary>
        /// 存成CSV
        /// </summary>
        /// <returns>是否儲存成功</returns>
        public override bool SaveCSV()
        {
            try
            {
                string filePath = $"Q2\\{this.TableName}";
                AddFolder(filePath);
                foreach (string year in RegexData.Keys)
                {
                    string rowName = "非營業日,公司代號,基金總數";
                    WriteToCSV($"{filePath}\\{year}.csv", RegexData[year], rowName);
                }
                return true;
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
                return false;
            }
        }

        /// <summary>
        /// 設定DataTable
        /// </summary>
        protected override void SetDataTable()
        {
            ReadDataTable = new DataTable();
            ReadDataTable.Columns.Add("非營業日", typeof(string));
            ReadDataTable.Columns.Add("公司代號", typeof(string));
            ReadDataTable.Columns.Add("基金總數", typeof(string));
            ReadDataTable.Columns.Add("CTIME", typeof(DateTime));
            ReadDataTable.Columns.Add("MTIME", typeof(int));
        }
    }
}
